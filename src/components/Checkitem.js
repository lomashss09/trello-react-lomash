import React, { Component } from "react";
export class Checkitem extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div>
            {this.props.checkitem.map(checkitem => (
              <div class ="board-name checkbox-checkitem">
               <input type="checkbox"/>
              <p >{checkitem.name}</p>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
export default Checkitem;
