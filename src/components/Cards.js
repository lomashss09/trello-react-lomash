import React, { Component } from 'react'
import {Link} from 'react-router-dom';
export class Cards extends Component {
    render() {
        return (
            <div>
            <div className="container">
            <div>{this.props.card.map((card)=>(
            <Link to="/list/card/checklist"><p class ="board-name" onClick={this.props.displayChecklist.bind(this,card.id)} id ="board-name">{card.name}</p></Link>))}
                </div>
          </div>
            </div>
        )
    }
}

export default Cards
