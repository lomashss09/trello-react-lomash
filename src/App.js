import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Header from './components/layouts/Header';
import Boards from "./components/Boards";
import Lists from "./components/Lists";
import Cards from "./components/Cards";
import Checklist from "./components/Checklist";
import Checkitem from "./components/Checkitem";
import Modal from 'react-modal';
const fetchBoard = require("./Fetchtrello").fetchBoard;
const fetchList = require("./Fetchtrello").fetchList;
const fetchCard = require("./Fetchtrello").fetchCard;
const fetchChecklist = require("./Fetchtrello").fetchChecklist;
const fetchCheckitem = require("./Fetchtrello").fetchCheckListItems;

class App extends Component {
  state = {
    board: [],
    displayBoard: false,
    list: [],
    card: [],
    checklist: [],
    checkitem: [],
    boardId:[],
    cardId:[]

  };

  async componentDidMount() {
    Modal.setAppElement('body');
    let boardItem = await fetchBoard();
    this.setState({ board: [...boardItem.map(boardName => boardName)] });
  }

  displayBoard = e => {
    this.setState({ displayBoard: !this.state.displayBoard });
  };

  displayList = async boardId => {
    this.setState({boardId:[boardId]})
    let lists = await fetchList(boardId);
    this.setState({ list: [...lists.map(listItem => listItem)] });
  };

  displayCards = async listId => {
    this.setState({cardId:[listId]})
    let cards = await fetchCard(listId);
    this.setState({ card: [...cards.map(card => card)] });
  };

  displayChecklist = async cardId => {
    let checklist = await fetchChecklist(cardId);
    this.setState({ checklist: [...checklist.map(checklist => checklist)] });
  };

  displayCheckitem = async checklistId => {
    let checkitem = await fetchCheckitem(checklistId);
    this.setState({ checkitem: [...checkitem.map(checkitem => checkitem)] });
  };

  render() {
    return (
      <Router>
        <div>
        <Header/>
          <Route
            exact
            path="/"
            render={props => (
              <React.Fragment>
                  <button
                    className="board-btn"
                    onClick={this.displayBoard}
                  >
                    Boards
                  </button>
                <Boards
                  board={this.state.board}
                  displayList={this.displayList}
                  todisplayBoard={this.state.displayBoard}
                />
              </React.Fragment>
            )}
          />
          <Route
            exact
            path={`/b/${this.state.boardId}`}
            render={props => (
              
              <React.Fragment>
                <Lists
                  list={this.state.list}
                  displayCards={this.displayCards}
                />
              </React.Fragment>
              
            )}
          />
          <Route
            exact
            path={`/c/${this.state.cardId}`}
            render={props => (
              <Modal isOpen={this.state.displayBoard} onRequestClose={this.displayBoard}>
              <React.Fragment>
                <Cards
                  card={this.state.card}
                  displayChecklist={this.displayChecklist}
                />
              </React.Fragment>
              </Modal>
            )}
          />
          <Route
            exact
            path="/list/card/checklist"
            render={props => (
              <React.Fragment>
                <Checklist
                  checklist={this.state.checklist}
                  displayCheckitem={this.displayCheckitem}
                />
              </React.Fragment>
            )}
          />
          <Route
            path="/list/card/checklist/checkitem"
            render={props => (
              <React.Fragment>
                <Checkitem checkitem={this.state.checkitem} />
              </React.Fragment>
            )}
          />
        </div>
      </Router>
    );
  }
}

export default App;
