import React, { Component } from 'react'
import {Link} from 'react-router-dom';
export class Checklist extends Component {
    render() {
        return (
            <div>
            <div className="container">
            <div>{this.props.checklist.map((checklist)=>(
            <Link to="/list/card/checklist/checkitem"><p class ="board-name" onClick={this.props.displayCheckitem.bind(this,checklist.id)}>{checklist.name}</p></Link>))}
                </div>
          </div>
            </div>
        )
    }
}

export default Checklist
