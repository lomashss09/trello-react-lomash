import React, { Component } from "react";
import { Link } from "react-router-dom";
class Boards extends Component {
  render() {
    if (this.props.todisplayBoard === false) {
      return <div></div>;
    }
    return (
      <div className="container">
        <div className="boards">
          {this.props.board.map(board => (
            <Link to={`/b/${board.id}`}>
              <p
                style={{
                  backgroundBottomColor:"#0e1211",
                  backgroundBrightness: "dark",
                  backgroundTopColor: "#191f1d",
                  backgroundImage: `url(${board.prefs.backgroundImage})`,
                  backgroundSize: "100% 100%",
                  height:150,
                  width:250,
                  borderRadius:10,
                  display:"inline-block"
                }}
                onClick={this.props.displayList.bind(this, board.id)}
                className="board-name"
              >
                {board.name}
              </p>
            </Link>
          ))}
        </div>
      </div>
    );
  }
}

export default Boards;
