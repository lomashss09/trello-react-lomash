import React, { Component } from "react";
import { Link } from "react-router-dom";
console.log("hell");
export class Lists extends Component {
  render() {
    return (
      <div className="container">
        <div className ="list">
          {this.props.list.map(list => (
            <Link to={`/c/${list.id}`}>
              <div className="list-items">
              <p className ="list-name"
                onClick={this.props.displayCards.bind(this, list.id)}
              >
                {list.name}
              </p></div>
            </Link>
          ))}
        </div>
      </div>
    );
  }
}

export default Lists;
