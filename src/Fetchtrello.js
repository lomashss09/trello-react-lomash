var apiKey ="e1514173e6d60e998cf9e6ff68a52870";
var token = "7a305a9008861e4746ec1521959515d29455c7feda4386dce933601e3380c9ed";
function fetchBoard(){
    return fetch('https://api.trello.com/1/members/me/boards?filter=all&fields=all&lists=none&memberships=none&organization=false&organization_fields=name%2CdisplayName&key='+apiKey+'&token='+token)
    .then((data)=> {return (data.json())})
}
function fetchList(boardId){
    return fetch('https://api.trello.com/1/boards/'+boardId+'/lists?cards=none&card_fields=all&filter=open&fields=all&key='+apiKey+'&token='+token)
    .then((data)=> {return (data.json())})
  }

function fetchCard(listId){
    return fetch('https://api.trello.com//1/lists/'+listId+'/cards?fields=id,name,badges,labels&key='+apiKey+'&token='+token)
    .then((data)=>{return data.json()})
}

function fetchChecklist(cardId) {
      return fetch(
        'https://api.trello.com/1/cards/'+cardId+'/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key='+apiKey+'&token='+token).then(data => data.json());
};

// Function to Fetch CheckList Items

function fetchCheckListItems(checkListId) {
    return new Promise(resolve => {
      fetch("https://api.trello.com/1/checklists/"+checkListId +"/checkItems?filter=all&fields=all&key="+apiKey +"&token=" +token).then(data => {
        resolve(data.json());
      });
    });
  }
module.exports = {fetchBoard, fetchList,fetchCard,fetchChecklist,fetchCheckListItems};